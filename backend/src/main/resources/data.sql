DROP TABLE IF EXISTS BUROS;

create table BUROS
(
    ID        BIGINT auto_increment,
    NAME      VARCHAR default 255,
    FULL_NAME VARCHAR default 255,
    CHIEF     BIGINT,
    STATE     VARCHAR default 32,
    constraint BUROS_PK
        primary key (ID)
);

ALTER TABLE BUROS
    ALTER COLUMN "STATE" SET DEFAULT 'ACTIVE';


DROP TABLE IF EXISTS ENGINEERS;

create table ENGINEERS
(
    ID         BIGINT auto_increment,
    LOGIN      VARCHAR default 100 unique,
    FIRST_NAME VARCHAR default 255,
    LAST_NAME  VARCHAR default 255,
    POSITION   VARCHAR default 255,
    STATE      VARCHAR default 50,
    BURO_ID    BIGINT,

    constraint ENGINEERS_PK
        primary key (ID),
    FOREIGN KEY (BURO_ID) REFERENCES BUROS (Id)
--     FOREIGN KEY (BURO_ID) REFERENCES BUROS (Id) ON DELETE SET null
);

ALTER TABLE ENGINEERS
    ALTER COLUMN "STATE" SET DEFAULT 'ACTIVE';
ALTER TABLE ENGINEERS
    ALTER COLUMN "POSITION" SET DEFAULT 'ENG';


DROP TABLE IF EXISTS WORKSHOPS;

CREATE TABLE WORKSHOPS
(
    ID        BIGINT AUTO_INCREMENT,
    NAME      VARCHAR default 50,
    FULL_NAME VARCHAR default 255,
    NUMBER    VARCHAR DEFAULT 30,

    CONSTRAINT WORKSHOPS_PK
        primary key (ID)
);

DROP TABLE IF EXISTS PROJECTS;

CREATE TABLE PROJECTS
(
    ID             BIGINT AUTO_INCREMENT,
    NAME           VARCHAR default 255,
    DESIGNATION    VARCHAR default 255,
    BASICS         VARCHAR default 255,
    DECREE         INTEGER,
    DECREE_SECTION INTEGER,
    WORKSHOP_ID    BIGINT,
    DEVELOPED_BY   VARCHAR default 50,

    CONSTRAINT PROJECTS_PK primary key (ID),
    FOREIGN KEY (WORKSHOP_ID) REFERENCES WORKSHOPS (ID)
);


-- INSERTING INITIAL DATA **********************************************************************************************

INSERT INTO BUROS
    (NAME, FULL_NAME, STATE)
VALUES ('УПиР', 'Управление Проектирования и Реконструкции', DEFAULT),
       ('АСБ', 'Архитектурно-строительное бюро', DEFAULT),
       ('БМП', 'Бюро метизного производства', DEFAULT),
       ('БПТО', 'Бюро подъемно-транспортного оборудования', DEFAULT);

INSERT INTO ENGINEERS
    (LOGIN, FIRST_NAME, LAST_NAME, POSITION, BURO_ID, STATE)
VALUES ('nicomed', 'Андрей', 'Пушков', 'GIP', 1, DEFAULT),
       ('yarik', 'Ярослав', 'Пушков', DEFAULT, 2, DEFAULT),
       ('sancho', 'Александр', 'Пушков', DEFAULT, 2, DEFAULT),
       ('mikchalych', 'Анатолий', 'Нагорный', DEFAULT, 2, 'DELETED');

UPDATE BUROS
SET CHIEF = 2
WHERE ID = 2;
UPDATE BUROS
SET CHIEF = 1
WHERE ID = 1;

INSERT INTO WORKSHOPS
    (NAME, FULL_NAME, NUMBER)
VALUES ('ЭнЦ', 'Энергетический цех', '1400'),
       ('СтПЦ-1', 'Сталепроволчный цех №1', '0600'),
       ('СтПЦ-2', 'Сталепроволчный цех №2', '0700'),
       ('СтПЦ-3', 'Сталепроволчный цех №3', '3100');

INSERT INTO PROJECTS
    (NAME, DESIGNATION, BASICS, DECREE, DECREE_SECTION, WORKSHOP_ID, DEVELOPED_BY)
VALUES ('Проект_1', 'Обозначение_1', 'ТЗ_1', 1, 1, 1, 'UPiR'),
       ('Проект_2', 'Обозначение_2', 'ТЗ_2', 3, 2, 2, 'UPiR'),
       ('Проект_3', 'Обозначение_3', 'ТЗ_3', 1, 3, 1, 'OUTSIDE'),
       ('Проект_4', 'Обозначение_4', 'ТЗ_4', 2, 4, 3, 'COOPERATIVE');


-- UPDATE ENGINEERS SET BURO_ID=NULL WHERE BURO_ID=2;DELETE FROM BUROS WHERE ID=2;
