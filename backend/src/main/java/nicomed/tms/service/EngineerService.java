package nicomed.tms.service;

import nicomed.tms.enums.State;
import nicomed.tms.model.Buro;
import nicomed.tms.model.Engineer;

import java.util.List;


public interface EngineerService {

    Engineer findById(Long id);
    Engineer findByLogin(String login);
    Engineer findByLastName(String lastName);
    boolean isEngineerLoginExists(String login);
    List<Engineer> findAll();
    void delete(Engineer engineer);
    void deleteById(Long id);
    Engineer save(Engineer engineer);

    List<Engineer> findAllByBuro(Buro buro);
}
