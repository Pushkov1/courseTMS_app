package nicomed.tms.service;

import nicomed.tms.enums.State;
import nicomed.tms.model.Buro;
import nicomed.tms.model.Engineer;
import nicomed.tms.repository.BuroRepository;
import nicomed.tms.repository.EngineersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

@Service
@Qualifier(value = "EngineerService")
public class EngineerServiceImpl implements EngineerService {

    private final EngineersRepository engineersRepository;

    @Autowired
    public EngineerServiceImpl(EngineersRepository engineersRepository) {
        this.engineersRepository = engineersRepository;
    }

    @Override
    public Engineer findById(Long id) {
        return engineersRepository.findByIdAndState(id, State.ACTIVE)
                .orElseThrow(() -> new NoSuchElementException("User with id:='" + id + "' not found"));
    }

    @Override
    public Engineer findByLogin(String login) {
        return engineersRepository.findByLoginAndState(login, State.ACTIVE)
                .orElseThrow(() -> new NoSuchElementException("User with login:'" + login + "' not found"));
    }

    @Override
    public Engineer findByLastName(String lastName) {
        return engineersRepository.findByLastNameAndState(lastName,State.ACTIVE)
                .orElseThrow(() -> new NoSuchElementException("User with last name:'" + lastName + "' not found"));
    }

    @Override
    public boolean isEngineerLoginExists(String login) {
            return  engineersRepository.findByLogin(login).isPresent();
    }

    @Override
    public List<Engineer> findAll() {
        return engineersRepository.findAllByState(State.ACTIVE);
    }

    @Override
    public void delete(Engineer engineer) {
        engineer.setState(State.DELETED);
        engineersRepository.save(engineer);
    }

    @Override
    public void deleteById(Long id) {
        Engineer engineer = engineersRepository.findById(id)
                .orElseThrow(() -> new NoSuchElementException("User with id:='" + id + "' not found"));
        engineer.setState(State.DELETED);
        engineersRepository.save(engineer);
    }

    @Override
    public Engineer save(Engineer engineer) {
        return engineersRepository.save(engineer);
    }

    @Override
    public List<Engineer> findAllByBuro(Buro buro) {
        return engineersRepository.findAllByBuroAndState(buro, State.ACTIVE);
    }
}
