package nicomed.tms.service;


import nicomed.tms.model.Buro;

import java.util.List;

public interface BuroService {

    Buro findById(Long id);
    Buro findByName(String name);
    Buro findByFullName(String fullName);
    List<Buro> findAll();
    void delete(Buro buro);
    void deleteById(Long id);

}
