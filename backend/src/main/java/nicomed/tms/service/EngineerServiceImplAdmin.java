package nicomed.tms.service;

import nicomed.tms.model.Buro;
import nicomed.tms.model.Engineer;
import nicomed.tms.repository.EngineersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

@Service
@Qualifier(value = "EngineerAdminService")
public class EngineerServiceImplAdmin implements EngineerService {

    private final EngineersRepository repo;

    @Autowired
    public EngineerServiceImplAdmin(EngineersRepository repo) {
        this.repo = repo;
    }

    @Override
    public Engineer findById(Long id) {
        return repo.findById(id)
                .orElseThrow(() -> new NoSuchElementException("User with id:='" + id + "' not found"));
    }

    @Override
    public Engineer findByLogin(String login) {
        return repo.findByLogin(login)
                .orElseThrow(() -> new NoSuchElementException("User with login:='" + login + "' not found"));
    }

    @Override
    public Engineer findByLastName(String lastName) {
        return repo.findByLastName(lastName)
                .orElseThrow(() -> new NoSuchElementException("User with last name:='" + lastName + "' not found"));
    }

    @Override
    public List<Engineer> findAll() {
        return repo.findAll();
    }

    @Override
    public void delete(Engineer engineer) {
        repo.delete(engineer);
    }

    @Override
    public void deleteById(Long id) {
        repo.deleteById(id);
    }

    @Override
    public Engineer save(Engineer engineer) {
           return repo.save(engineer);
    }

    @Override
    public boolean isEngineerLoginExists(String login) {
        return  repo.findByLogin(login).isPresent();
    }

    @Override
    public List<Engineer> findAllByBuro(Buro buro) {
        return repo.findAllByBuro(buro);
    }
}
