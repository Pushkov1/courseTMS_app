package nicomed.tms.service;

import nicomed.tms.model.Buro;
import nicomed.tms.repository.BuroRepository;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.logging.Logger;

@Service
@Qualifier(value = "BuroAdminService")
public class BuroServiceImplAdmin implements BuroService {

    private static Logger log = Logger.getLogger(BuroServiceImplAdmin.class.getName());

    private final BuroRepository buroRepository;

    public BuroServiceImplAdmin(BuroRepository repo) {
        this.buroRepository = repo;
    }

    @Override
    public Buro findById(Long id) {
        return buroRepository.findById(id)
                .orElseThrow(() -> new NoSuchElementException("Buro with id='" + id + "' not found"));
    }

    @Override
    public Buro findByName(String name) {
        return buroRepository.findByName(name)
                .orElseThrow(() -> new NoSuchElementException("Buro with name='" + name + "' not found"));
    }

    @Override
    public Buro findByFullName(String fullName) {
        return buroRepository.findByFullName(fullName)
                .orElseThrow(() -> new NoSuchElementException("Buro with full name='" + fullName + "' not found"));
    }

    @Override
    public List<Buro> findAll() {
        return buroRepository.findAll();
    }

    @Override
    public void delete(Buro buro) {
        buroRepository.delete(buro);
    }

    @Override
    public void deleteById(Long id) {
            buroRepository.deleteById(id);
    }


}
