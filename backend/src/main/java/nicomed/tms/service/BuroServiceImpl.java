package nicomed.tms.service;

import nicomed.tms.enums.State;
import nicomed.tms.model.Buro;
import nicomed.tms.repository.BuroRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

@Service
@Qualifier(value = "BuroService")
public class BuroServiceImpl implements BuroService {

    private final BuroRepository buroRepository;

    @Autowired
    public BuroServiceImpl(BuroRepository buroRepository) {
        this.buroRepository = buroRepository;
    }

    @Override
    public Buro findById(Long id) {
        return buroRepository.findByIdAndState(id,State.ACTIVE)
                .orElseThrow(() -> new NoSuchElementException("Buro with id:" + id + " not found"));
    }

    @Override
    public Buro findByName(String name) {
        return buroRepository.findByNameAndState(name, State.ACTIVE)
                .orElseThrow(() -> new NoSuchElementException("Buro with name:" + name + " not found"));
    }

    @Override
    public Buro findByFullName(String fullName) {
        return buroRepository.findByFullNameAndState(fullName, State.ACTIVE)
                .orElseThrow(() -> new NoSuchElementException("Buro with full name:" + fullName + " not found"));
    }

    @Override
    public List<Buro> findAll() {
        return buroRepository.findAllByState(State.ACTIVE);
    }

    @Override
    public void delete(Buro buro) {
        buro.setState(State.DELETED);
        buroRepository.save(buro);
    }

    @Override
    public void deleteById(Long id) {
        Buro buro = buroRepository.findByIdAndState(id, State.ACTIVE)
                .orElseThrow(() -> new NoSuchElementException("Buro with id:" + id + " not found"));
        buro.setState(State.DELETED);
        buroRepository.save(buro);
    }
}
