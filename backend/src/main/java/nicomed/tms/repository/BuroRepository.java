package nicomed.tms.repository;

import nicomed.tms.enums.State;
import nicomed.tms.model.Buro;
import nicomed.tms.model.Engineer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BuroRepository extends JpaRepository<Buro, Long> {

    Optional<Buro> findByName(String name);
    Optional<Buro> findByFullName(String fullName);

    List<Buro> findAllByState(State state);
    Optional<Buro> findByIdAndState(Long id,State state);
    Optional<Buro> findByNameAndState(String name,State state);
    Optional<Buro> findByFullNameAndState(String fullName,State state);

    /**  КАК ЗАСТАВИТЬ ЭТО РАБОТАТЬ **/
    /*
    @Modifying
    @Query(
            value = "UPDATE ENGINEERS e SET e.buro_id = NULL WHERE e.buro_id = :id; DELETE FROM BUROS b  WHERE b.id = :id;",
            nativeQuery = true
    )
    void clearDependedFieldBeforeDelete(@Param("id") Long id);
    */


}
