package nicomed.tms.repository;

import nicomed.tms.enums.State;
import nicomed.tms.model.Buro;
import nicomed.tms.model.Engineer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EngineersRepository extends JpaRepository<Engineer, Long> {

    Optional<Engineer> findByLogin(String login);
    Optional<Engineer> findByLastName(String lastName);
    List<Engineer> findAllByBuro(Buro buro);

    List<Engineer> findAllByState(State state);
    Optional<Engineer> findByIdAndState(Long id, State state);
    Optional<Engineer> findByLoginAndState(String login, State state);
    Optional<Engineer> findByLastNameAndState(String lastName, State state);
    List<Engineer> findAllByBuroAndState(Buro buro, State state);
}
