package nicomed.tms.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import nicomed.tms.enums.Position;
import nicomed.tms.enums.State;

import javax.persistence.*;

@Data
@ToString(exclude = "buro")
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="engineers")
public class Engineer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String login;
    private String firstName;
    private String lastName;


    @ManyToOne
    @JoinColumn(name = "buro_id")
//    @JsonManagedReference
    private Buro buro;


    @Enumerated(value = EnumType.STRING)
    private Position position;

//    @Column(length = 32, columnDefinition = "varchar(32) default 'ACTIVE'")
    @Enumerated(value = EnumType.STRING)
    private State state;

}
