package nicomed.tms.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import nicomed.tms.enums.State;

import javax.persistence.*;
import java.util.List;

@Data
@ToString(exclude = {"workers","chief"})
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "buros")
public class Buro {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private String fullName;

    @OneToMany(mappedBy = "buro")
//    @JsonBackReference
    private List<Engineer> workers;

    @OneToOne
    @JoinColumn(name = "chief")
    @JsonBackReference
    private Engineer chief;

    @Enumerated(value = EnumType.STRING)
    private State state;
}
