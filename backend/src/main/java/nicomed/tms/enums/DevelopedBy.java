package nicomed.tms.enums;

public enum DevelopedBy {
    UPiR,
    OUTSIDE,
    COOPERATIVE
}
