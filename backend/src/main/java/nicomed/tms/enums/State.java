package nicomed.tms.enums;

public enum State {
    ACTIVE,
    DISABLED,
    DELETED
}
