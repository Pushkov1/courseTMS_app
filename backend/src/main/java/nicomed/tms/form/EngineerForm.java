package nicomed.tms.form;

import lombok.*;
import nicomed.tms.enums.Position;
import nicomed.tms.model.Buro;
import nicomed.tms.model.Engineer;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class EngineerForm {

    private Long id;
    private String login;
    private String firstName;
    private String lastName;
    private String position;
    private Long buroId;


    public static EngineerForm fromEngineer(Engineer eng) {
        return EngineerForm.builder()
                .id(eng.getId())
                .login(eng.getLogin())
                .firstName(eng.getFirstName())
                .lastName(eng.getLastName())
                .position(eng.getPosition().toString())
                .buroId(
                        eng.getBuro() != null ? eng.getBuro().getId() : -1
                )
                .build();
    }

    public static Engineer updateEngineer(Engineer eng, EngineerForm form, Buro buro) {
        eng.setLogin(form.getLogin());
        eng.setFirstName(form.getFirstName());
        eng.setLastName(form.getLastName());
        eng.setPosition(
                form.getPosition() != null ? Position.valueOf(form.getPosition()) : Position.ENG
        );
        eng.setBuro(buro);
        return eng;
    }


}
