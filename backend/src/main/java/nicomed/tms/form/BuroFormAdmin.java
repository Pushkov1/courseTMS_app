package nicomed.tms.form;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import nicomed.tms.model.Buro;

import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BuroFormAdmin {

    private Long id;
    private String name;
    private String fullName;
    private List<EngineerForm> workers;

    public static BuroFormAdmin from(Buro buro) {
        return BuroFormAdmin.builder()
                .id(buro.getId())
                .name(buro.getName())
                .fullName(buro.getFullName())
                .workers(buro.getWorkers()
                        .stream()
                        .map(EngineerForm::fromEngineer)
                        .collect(Collectors.toList()))
                .build();
    }

    public static Buro createBuro(BuroFormAdmin form) {
        Buro buro = new Buro();
        buro.setName(form.name);
        buro.setFullName(form.fullName);
        return buro;
    }

    public static Buro updateBuro(Buro buro, BuroFormAdmin dto){
        buro.setName(dto.getName());
        buro.setFullName(dto.getFullName());
        return buro;
    }
}
