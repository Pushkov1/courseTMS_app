package nicomed.tms.form;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import nicomed.tms.enums.State;
import nicomed.tms.model.Buro;

import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BuroForm {

    private Long id;
    private String name;
    private String fullName;
    private List<EngineerForm> workers;

    public static BuroForm from(Buro buro) {
        return BuroForm.builder()
                .id(buro.getId())
                .name(buro.getName())
                .fullName(buro.getFullName())
                .workers(buro.getWorkers()
                        .stream()
                        .filter(b -> b.getState().equals(State.ACTIVE))
                        .map(EngineerForm::fromEngineer)
                        .collect(Collectors.toList()))
                .build();
    }

    public static Buro createBuro(BuroForm form) {
        Buro buro = new Buro();
        buro.setName(form.name);
        buro.setFullName(form.fullName);
        return buro;
    }

    public static Buro updateBuro(Buro buro, BuroForm dto){
        buro.setName(dto.getName());
        buro.setFullName(dto.getFullName());
        return buro;
    }
}
