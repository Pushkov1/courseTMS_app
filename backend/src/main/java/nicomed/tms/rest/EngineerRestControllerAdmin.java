package nicomed.tms.rest;

import nicomed.tms.enums.State;
import nicomed.tms.form.EngineerForm;
import nicomed.tms.model.Buro;
import nicomed.tms.model.Engineer;
import nicomed.tms.service.BuroService;
import nicomed.tms.service.EngineerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.http.HttpRequest;
import java.util.NoSuchElementException;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/tms/api/admin/engineers")
public class EngineerRestControllerAdmin {

    private static Logger log = Logger.getLogger(EngineerRestControllerAdmin.class.getName());

    private final EngineerService engineerService;
    private final BuroService buroService;

    @Autowired
    public EngineerRestControllerAdmin(
            @Qualifier("EngineerAdminService") EngineerService engineerService,
            @Qualifier("BuroAdminService") BuroService buroService) {
        this.engineerService = engineerService;
        this.buroService = buroService;
    }

    @GetMapping("")
    public ResponseEntity<?> getAllEngineers() {
        return new ResponseEntity<>(
                engineerService.findAll()
                        .stream()
                        .map(EngineerForm::fromEngineer)
                        .collect(Collectors.toList()),
                HttpStatus.OK);
    }

    @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> createEngineer(@RequestBody EngineerForm engineerDTO) {
        if (!engineerService.isEngineerLoginExists(engineerDTO.getLogin())) {
            Engineer engineer = new Engineer();
            engineer = EngineerForm.updateEngineer(engineer, engineerDTO, null);
            engineer.setState(State.ACTIVE);
            engineerService.save(engineer);
            return new ResponseEntity<>(engineer, HttpStatus.OK);
        }
        return new ResponseEntity<>("Login is exits", HttpStatus.BAD_REQUEST);
    }


    @GetMapping("/{id}")
    public ResponseEntity<?> getEngineerById(@PathVariable("id") Long id) {
        try {
            return new ResponseEntity<>(
                    EngineerForm.fromEngineer(engineerService.findById(id))
                    , HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>("Engineer not found", HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateEngineer(@PathVariable("id") Long id, @RequestBody EngineerForm engineerDTO) {
        try {
            Engineer engineer = engineerService.findById(id);
            engineer = EngineerForm.updateEngineer(engineer, engineerDTO, null);
            engineerService.save(engineer);
            return new ResponseEntity<>(EngineerForm.fromEngineer(engineer), HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteEngineerById(@PathVariable("id") Long id) {
        try {
            Engineer engineer = engineerService.findById(id);
            engineerService.delete(engineer);
            return new ResponseEntity<>(
                    "Engineer with id:" + id + " deleted"
                    , HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>("Engineer not found", HttpStatus.NOT_FOUND);
        }
    }

}
