package nicomed.tms.rest;

import nicomed.tms.form.EngineerForm;
import nicomed.tms.model.Buro;
import nicomed.tms.model.Engineer;
import nicomed.tms.service.BuroService;
import nicomed.tms.service.EngineerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.NoSuchElementException;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/tms/api/engineers")
public class EngineerRestController {

    private static Logger log = Logger.getLogger(EngineerRestController.class.getName());

    private final EngineerService engineerService;
    private final BuroService buroService;

    @Autowired
    public EngineerRestController(
            @Qualifier("EngineerService") EngineerService engineerService,
            @Qualifier("BuroService") BuroService buroService) {
        this.engineerService = engineerService;
        this.buroService = buroService;
    }

    @GetMapping("")
    public ResponseEntity<?> getAllEngineers() {
        return new ResponseEntity<>(
                engineerService.findAll()
                        .stream()
                        .map(EngineerForm::fromEngineer)
                        .collect(Collectors.toList()),
                HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getEngineerById(@PathVariable("id") Long id) {
        try {
            return new ResponseEntity<>(
                    EngineerForm.fromEngineer(engineerService.findById(id))
                    , HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>("Engineer not found", HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateEngineer(@PathVariable("id") Long id, @RequestBody EngineerForm engineerDTO) {
        try {
            Engineer engineer = engineerService.findById(id);
            Buro buro = buroService.findById(id);
            engineer = EngineerForm.updateEngineer(engineer, engineerDTO, buro);
            engineerService.save(engineer);
            return new ResponseEntity<>(EngineerForm.fromEngineer(engineer), HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
