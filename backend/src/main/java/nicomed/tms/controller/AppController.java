package nicomed.tms.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.logging.Logger;

@Controller
@RequestMapping("/")
public class AppController {

    private static Logger log = Logger.getLogger(AppController.class.getName());

    @GetMapping("")
    public String getEmptyIndex(){
        return "redirect:/tms/index";
    }
    @GetMapping("/tms")
    public String getIndexTMS(){
        return "redirect:/tms/index";
    }

    @GetMapping("/tms/index")
    public String getIndex(){
        return "index";
    }

    @GetMapping("/tms/users")
    public String getUsers() {
        return "index";
    }

    @GetMapping("/tms/about")
    public String getAboutPage(){
        return "index";
    }
}
